package group.backend.response;

public interface PlayerView {
    Long getId();
    String getUsername();
}