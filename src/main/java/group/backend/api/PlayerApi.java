package group.backend.api;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import group.backend.entity.Player;
import group.backend.repository.PlayerRepository;
import group.backend.request.PlayerForm;
import group.backend.response.PlayerView;



@RestController
@RequestMapping(value = "/players", produces = MediaType.APPLICATION_JSON_VALUE)
public class PlayerApi {

    @Autowired
    private PlayerRepository playerRepository;
    
    @GetMapping(value = "/all", consumes = MediaType.ALL_VALUE)
    public List<PlayerView> getAll() {
        return playerRepository.findAllViewsBy();
    }

    @GetMapping(value = "/{id}", consumes = MediaType.ALL_VALUE)
    public PlayerView getOne(@PathVariable Long id) {
        return playerRepository.findViewById(id);
    }

    @PutMapping(value = "/insert", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void insertOne(@RequestBody @Valid PlayerForm form) {
        Player player = new Player();
        player.setUsername(form.getUsername());
        playerRepository.save(player);
    }

    @PatchMapping(value = "/update/{id}")
    public void updateOne(@PathVariable Long id, @RequestBody PlayerForm form) {
        Optional<Player> optional = playerRepository.findById(id);
        if (optional.isPresent()) {
            Player player = optional.get();
            player.setUsername(form.getUsername());
            playerRepository.save(player);
        }
    }

    @DeleteMapping(value = "/delete/{id}", consumes = MediaType.ALL_VALUE)
    public void deleteOne(@PathVariable Long id) {
        playerRepository.deleteById(id);
    }
}