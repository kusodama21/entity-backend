package group.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import group.backend.entity.Player;
import group.backend.response.PlayerView;

@Repository
@Transactional
public interface PlayerRepository extends JpaRepository<Player, Long> {
    public PlayerView findViewById(Long id);
    public List<PlayerView> findAllViewsBy();
}